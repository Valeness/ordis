package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"time"
	"fmt"
)

type BuyData struct {
	Max string `json:"max"`
	Average string `json:"weightedAverage"`
}

type Item struct {
	Buy BuyData `json:"buy"`
}

type Post struct {
	Id int `json:"id"`
	User_id int `json:"user_id"`
	Body string `json:"body"`
}

func main() {

	url := "https://market.fuzzwork.co.uk/aggregates/?region=10000002&types=34"

	spaceClient := http.Client{
		Timeout: time.Second * 2, // Maximum of 2 secs
	}

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("User-Agent", "go test")

	res, getErr := spaceClient.Do(req)
	if getErr != nil {
		log.Fatal(getErr)
	}

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Fatal(readErr)
	}

	items := make(map[int]*Item)
	jsonErr := json.Unmarshal(body, &items)

	if jsonErr != nil {
		log.Fatal(jsonErr)
	}

	for _, d := range items {
		fmt.Printf("Buy: %s \n", d.Buy.Average)
	}
}